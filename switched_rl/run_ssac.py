from multiprocessing import Process
from seagul.rl.run_utils import run_sg
from seagul.rl.sac.sacn_adaptive import sac_switched
from seagul.rl.sac.models import SACModelSwitch, control
import torch
import torch.nn as nn
from seagul.nn import MLP
import numpy as np
from scipy.stats import multivariate_normal
from gym.envs.registration import register
import make_figures

input_size = 4
output_size = 1
layer_size = 32
num_layers = 4
activation = nn.ReLU

m1 = 1; m2 = 1
l1 = 1; l2 = 1
lc1 = .5; lc2 = .5
I1 = .2; I2 = 1.0
g = 9.8
max_torque = 25
lqr_max_torque = 25
max_t = 10.0


def reward_fn_sin(s, a):
    reward = (np.sin(s[0]) + np.sin(s[0] + s[1]))
    return reward, False


def reward_fn_gauss(s, a):
    return multivariate_normal.pdf(s, mean=[np.pi / 2, 0, 0, 0], cov=[1, 1, 1, 1]), False


def reward_fn(s, a):
    reward = -.1*(np.sqrt((s[0] - np.pi/2)**2 + s[1]**2))
    return reward, False


def reward_fn_sq(s, a):
    reward = -.1*((s[0] - np.pi/2)**2 + s[1]**2)
    return reward, False


env_config = {
    "init_state": [-np.pi / 2, 0, 0, 0],
    "max_torque": max_torque,
    "init_state_weights": [np.pi, np.pi, 0, 0],
    "dt": .01,
    "reward_fn": reward_fn_sq,
    "max_t": max_t,
    "m2": m2,
    "m1": m1,
    "l1": l1,
    "lc1": lc1,
    "lc2": lc2,
    "i1": I1,
    "i2": I2,
    #"integrator" : euler
}

register(id="su_acrobot-v0", entry_point='envs.acrobot:SGAcroEnv')
def run_ssac(trial_name, env_config, model, gate_update_freq=2500):
    proc_list = []
    #for seed in np.random.randint(0, 2 ** 31, 8):
    for seed in np.random.randint(0, 2 ** 31, 1):
        alg_config = {
            "env_name": "su_acrobot-v0",
            "total_steps": 6000,#1e5,
            "model": model,
            "seed": seed,
            "goal_state": torch.tensor([np.pi / 2, 0, 0, 0]),
            "goal_lookback": 10,
            "goal_thresh": .25,
            "alpha": 0.05,
            "needle_lookup_prob": .8,
            "exploration_steps": 5000,
            "gate_update_freq": gate_update_freq,
            "gate_x": torch.as_tensor(torch.load('gate_data/X_big')),
            "gate_y": torch.as_tensor(torch.load('gate_data/Y_big')),
            "env_config": env_config,
            "min_steps_per_update" : 500,
            "sgd_batch_size": 128,
            "replay_batch_size" : 4096,
            "iters_per_update": 16,
            "use_gpu": False,
            "gamma" : .99,
            "replay_buf_size" : int(50000),
        }
        print("run_sg")

        run_sg(alg_config, sac_switched, "trial" + str(seed), "", "/data_needle/" + trial_name + "/")
        p = Process(
            target=run_sg,
            args=(alg_config, sac_switched, "trial" + str(seed), "", "/data_needle/" + trial_name + "/"),
        )
        p.start()
        proc_list.append(p)

    for p in proc_list:
        print("joining")
        p.join()


if __name__ == "__main__":
    #gate_update_freq = [2500]
    #for g in gate_update_freq:
    #    trial_name = f"test2"
    #    run_ssac(trial_name, env_config, g)
    import sys, os


    """
    device = "cuda"
    policy = MLP(input_size, output_size * 2, num_layers, layer_size, activation)
    value_fn = MLP(input_size, 1, num_layers, layer_size, activation)
    q1_fn = MLP(input_size + output_size, 1, num_layers, layer_size, activation)
    q2_fn = MLP(input_size + output_size, 1, num_layers, layer_size, activation)
    gate_fn = torch.load("gate_data/gate_big")
    gate_fn.state_std = gate_fn.state_var
    model = SACModelSwitch(policy, value_fn, q1_fn, q2_fn, 25, balance_controller=control
                           , hold_count=20, gate_fn=gate_fn)

    run_ssac("test3", env_config, model, g)


    """
    trial_dir = os.path.join("data_needle", "test3")
    if trial_dir.startswith("\\"):
        trial_dir = trial_dir[2:]
    #workspace_path = os.path.join(os.path.dirname(__file__), trial_dir, "trial2016974998--10-9_22-7")
    make_figures.run(trial_dir, "su_acrobot-v0", env_config, SACModelSwitch, input_size = 4,
                        output_size = 1,
                        layer_size = 32,
                        num_layers = 4,
                        activation = nn.ReLU)

